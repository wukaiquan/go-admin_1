package service

import (
	"errors"
	"fmt"

	"github.com/go-admin-team/go-admin-core/sdk/runtime"
	"github.com/go-admin-team/go-admin-core/sdk/service"
	"gorm.io/gorm"

	"go-admin/app/admin/models"
	"go-admin/app/admin/service/dto"
	"go-admin/common/actions"
	cDto "go-admin/common/dto"
	"go-admin/common/global"
)

type SysApi struct {
	service.Service
}

// GetPage 获取SysApi列表
// 传进来的是指针，返回值为空，如何返回的？ 通过指针， list设置值，不用返回，传进来一个空的list，list赋值，返回err为空，证明代码正确,否则，查询出错。
// list是[]models.SysApi 类型指针
func (e *SysApi) GetPage(c *dto.SysApiGetPageReq, p *actions.DataPermission, list *[]models.SysApi, count *int64) error {
	var err error
	var data models.SysApi

	orm := e.Orm.Debug().Model(&data).
		Scopes(
			cDto.MakeCondition(c.GetNeedSearch()),
			cDto.Paginate(c.GetPageSize(), c.GetPageIndex()),
			actions.Permission(data.TableName(), p),
		)
	if c.Type != "" {
		qType := c.Type
		if qType == "暂无" {
			qType = ""
		}
		if global.Driver == "postgres" {
			orm = orm.Where("type = ?", qType)
		} else {
			orm = orm.Where("`type` = ?", qType)
		}

	}
	//orm.Find(list): 这里，Find 方法会执行查询并将结果填充到 list 指向的切片中。因为 list 是一个指针，所以 GORM 会解引用这个指针并直接修改它所指向的切片。
	//.Limit(-1) 和 .Offset(-1): 在大多数情况下，设置 Limit 和 Offset 为 -1 是不必要的，因为这会取消之前的分页设置（如果有的话）。在 GORM 中，设置这两个值为 -1 通常表示不使用限制或偏移，但这取决于 GORM 的具体实现和版本。通常，分页设置应该在调用 Find 方法之前通过 Paginate 或其他方法完成。
	//.Count(count): 这个方法会计算查询结果的总数，并将这个数赋值给 count 指向的变量。同样地，因为 count 是一个指针，所以 GORM 会直接修改它所指向的变量。
	//.Error: 这是 GORM 查询链的最后一个方法，它返回查询过程中遇到的任何错误（如果有的话）。这个属性是一个 error 类型的值，可以直接用于错误处理。
	err = orm.Find(list).Limit(-1).Offset(-1).
		Count(count).Error
	if err != nil {
		e.Log.Errorf("Service GetSysApiPage error:%s", err)
		return err
	}
	return nil
}

// Get 获取SysApi对象with id
func (e *SysApi) Get(d *dto.SysApiGetReq, p *actions.DataPermission, model *models.SysApi) *SysApi {
	var data models.SysApi
	err := e.Orm.Model(&data).
		Scopes(
			actions.Permission(data.TableName(), p),
		).
		First(model, d.GetId()).Error
	if err != nil && errors.Is(err, gorm.ErrRecordNotFound) {
		err = errors.New("查看对象不存在或无权查看")
		e.Log.Errorf("Service GetSysApi error:%s", err)
		_ = e.AddError(err)
		return e
	}
	if err != nil {
		e.Log.Errorf("db error:%s", err)
		_ = e.AddError(err)
		return e
	}
	return e
}

// Update 修改SysApi对象
func (e *SysApi) Update(c *dto.SysApiUpdateReq, p *actions.DataPermission) error {
	var model = models.SysApi{}
	db := e.Orm.Debug().First(&model, c.GetId())
	if db.RowsAffected == 0 {
		return errors.New("无权更新该数据")
	}
	c.Generate(&model)
	db = e.Orm.Save(&model)
	if err := db.Error; err != nil {
		e.Log.Errorf("Service UpdateSysApi error:%s", err)
		return err
	}

	return nil
}

// Remove 删除SysApi
func (e *SysApi) Remove(d *dto.SysApiDeleteReq, p *actions.DataPermission) error {
	var data models.SysApi

	db := e.Orm.Model(&data).
		Scopes(
			actions.Permission(data.TableName(), p),
		).Delete(&data, d.GetId())
	if err := db.Error; err != nil {
		e.Log.Errorf("Service RemoveSysApi error:%s", err)
		return err
	}
	if db.RowsAffected == 0 {
		return errors.New("无权删除该数据")
	}
	return nil
}

// CheckStorageSysApi 创建SysApi对象
func (e *SysApi) CheckStorageSysApi(c *[]runtime.Router) error {
	for _, v := range *c {
		err := e.Orm.Debug().Where(models.SysApi{Path: v.RelativePath, Action: v.HttpMethod}).
			Attrs(models.SysApi{Handle: v.Handler}).
			FirstOrCreate(&models.SysApi{}).Error
		if err != nil {
			err := fmt.Errorf("Service CheckStorageSysApi error: %s \r\n ", err.Error())
			return err
		}
	}
	return nil
}
