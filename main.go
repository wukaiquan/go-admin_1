package main

import (
	"go-admin/cmd"
)

//go:generate swag init --parseDependency --parseDepth=6 --instanceName admin -o ./docs/admin

// @title go-admin API
// @version 2.0.0
// @description 基于Gin + Vue + Element UI的前后端分离权限管理系统的接口文档
// @description 添加qq群: 521386980 进入技术交流群 请先star，谢谢！
// @license.name MIT
// @license.url https://github.com/go-admin-team/go-admin/blob/master/LICENSE.md

// @securityDefinitions.apikey Bearer
// @in header
// @name Authorization
// 初始化数据库 go run main.go  migrate -c config\settings.dev.yml
// go mod download 或者go mod tidy
// 启动  go run main.go server -c config/settings.dev.yml

// 为啥go run main.go server -c config/settings.dev.yml项目启动了?
// 1.包引用：main.go 导入了 cmd 包（尽管这里显示的是 go-admin/cmd，但实际的包路径可能不同），这个包应该包含了 server.go 文件或对其的引用。
// 2.全局变量和 init() 函数：在 server.go 文件中，定义了一个全局的 StartCmd 变量，它是 cobra.Command 的一个实例。这个变量在 init() 函数中被初始化和配置，包括添加命令行标志（flags）。
// 3.命令行参数处理：当您运行 go run main.go server -c config/settings.yml 时，cobra 会根据 StartCmd 的定义解析这些命令行参数，并相应地设置 configYml 和 apiCheck 变量。
// 4.setup() 和 run() 函数：在 StartCmd 的 PreRun 和 RunE 字段中，分别调用了 setup() 和 run() 函数。setup() 函数在命令行参数解析之后但在 run() 函数之前被调用，用于进行一些初始化设置，如读取配置文件、注册监听函数等。而 run() 函数则负责启动服务器或执行其他主要的程序逻辑。
// 5.AppRouters：在 server.go 中还定义了一个全局的 AppRouters 切片，并在 init() 函数中向其中添加了一个路由初始化函数。这可能是一个用于注册所有 API 路由的地方，尽管具体的路由实现和注册细节并没有在您提供的代码中展示。
func main() {
	cmd.Execute()
}
